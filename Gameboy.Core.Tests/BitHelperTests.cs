﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Gameboy.Core.Tests
{
    public class BitHelperTests
    {
        [Test]
        public void To16BitTest()
        {
            byte higher = 0x32;
            byte lower = 0xAF;
            var combined = BitHelpers.To16Bit(higher, lower);
            Assert.AreEqual(combined, 0x32AF);
        }

        [Test]
        public void SetBitTest()
        {
            byte value = 0x00;
            var newValue = BitHelpers.SetBit(value, 0);
            Assert.AreEqual(newValue, 0x1);

            newValue = BitHelpers.SetBit(newValue, 4);
            Assert.AreEqual(newValue, 0x11);

            newValue = BitHelpers.SetBit(newValue, 7);
            Assert.AreEqual(newValue, 0x91);
        }

        [Test]
        public void SetFlagsTest()
        {
            byte value = 0x00;
            var newValue = BitHelpers.SetFlag(value, Flags.C);
            Assert.AreEqual(newValue, 0x10);
            
            newValue = BitHelpers.SetFlag(value, Flags.H);
            Assert.AreEqual(newValue, 0x20);

            newValue = BitHelpers.SetFlag(value, Flags.N);
            Assert.AreEqual(newValue, 0x40);

            newValue = BitHelpers.SetFlag(value, Flags.Z);
            Assert.AreEqual(newValue, 0x80);

            newValue = BitHelpers.SetFlag(value, Flags.C);
            newValue += BitHelpers.SetFlag(value, Flags.H);
            newValue += BitHelpers.SetFlag(value, Flags.N);
            newValue += BitHelpers.SetFlag(value, Flags.Z);
            Assert.AreEqual(newValue, 0xF0);
        }

        [Test]
        public void ClearBitTest()
        {
            byte value = 0xFF;
            var newValue = BitHelpers.ClearBit(value, 0);
            Assert.AreEqual(newValue, 0xFE);

            newValue = BitHelpers.ClearBit(newValue, 4);
            Assert.AreEqual(newValue, 0xEE);

            newValue = BitHelpers.ClearBit(newValue, 7);
            Assert.AreEqual(newValue, 0x6E);
        }

        [Test]
        public void GetBitTest()
        {
            byte value = 0xAF;
            var bit = BitHelpers.GetBit(value, 7);
            Assert.AreEqual(bit, true);

            bit = BitHelpers.GetBit(value, 6);
            Assert.AreEqual(bit, false);
        }

        [TestCase((ushort)153, 0)]
        [TestCase((ushort)10, 0)]
        [TestCase((ushort)65535, 255)]
        [TestCase((ushort)45312, 177)]
        public void GetHighByteTest(ushort value, byte expected)
        {
            Assert.AreEqual(BitHelpers.GetHighBytes(value), expected);
        }

        [TestCase((ushort)45368, 56)]
        [TestCase((ushort)48639, 255)]
        [TestCase((ushort)14847, 255)]
        [TestCase((ushort)14771, 179)]
        [TestCase((ushort)153, 153)]
        [TestCase((ushort)10, 10)]
        public void GetLowerByteTest(ushort value, byte expected)
        {
            Assert.AreEqual(BitHelpers.GetLowerBytes(value), expected);
        }
    }
}
