﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Gameboy.Core.Tests
{
    public class MMUTests
    {
        private MMU m_mmu;
        private Cartridge m_cart;
        private GPU m_gpu;
        private RAM m_ram;
        private IO m_io;

        [SetUp]
        public void SetUp()
        {
            m_cart = new Cartridge("");
            m_gpu = new GPU();
            m_ram = new RAM();
            m_io = new IO();
            m_mmu = new MMU(m_cart, m_gpu, m_ram, m_io);
        }

        [Test]
        public void CartridgeMemory_ReadWriteTest()
        {
            // RomBank 1
            m_mmu.Write(0x0000, 1);
            m_mmu.Write(0x3FFF, 2);

            var romBank = TestHelpers.GetPrivateField<Cartridge, byte[]>(m_cart, "m_romBank");

            Assert.AreEqual(romBank[0], 1);
            Assert.AreEqual(romBank[^1], 2);

            // RomBank 2
            m_mmu.Write(0x4000, 3);
            m_mmu.Write(0x7FFF, 4);

            var romBank2 = TestHelpers.GetPrivateField<Cartridge, byte[]>(m_cart, "m_romBank2");

            Assert.AreEqual(romBank2[0], 3);
            Assert.AreEqual(romBank2[^1], 4);

            // Extern RAM
            m_mmu.Write(0xA000, 5);
            m_mmu.Write(0xBFFF, 6);

            var externRam = TestHelpers.GetPrivateField<Cartridge, byte[]>(m_cart, "m_externRam");

            Assert.AreEqual(externRam[0], 5);
            Assert.AreEqual(externRam[^1], 6);

            // MMU Read
            Assert.AreEqual(m_mmu.Read(0x0000), 1);
            Assert.AreEqual(m_mmu.Read(0x3FFF), 2);
            Assert.AreEqual(m_mmu.Read(0x4000), 3);
            Assert.AreEqual(m_mmu.Read(0x7FFF), 4);
            Assert.AreEqual(m_mmu.Read(0xA000), 5);
            Assert.AreEqual(m_mmu.Read(0xBFFF), 6);
        }

        [Test]
        public void GpuMemory_ReadWriteTest()
        {
            // vRAM
            m_mmu.Write(0x8000, 1);
            m_mmu.Write(0x9FFF, 2);

            var vram = TestHelpers.GetPrivateField<GPU, byte[]>(m_gpu, "m_vram");

            Assert.AreEqual(vram[0], 1);
            Assert.AreEqual(vram[^1], 2);

            // OAM
            m_mmu.Write(0xFE00, 3);
            m_mmu.Write(0xFE9F, 4);

            var oam = TestHelpers.GetPrivateField<GPU, byte[]>(m_gpu, "m_oam");

            Assert.AreEqual(oam[0], 3);
            Assert.AreEqual(oam[^1], 4);

            // MMU Read
            Assert.AreEqual(m_mmu.Read(0x8000), 1);
            Assert.AreEqual(m_mmu.Read(0x9FFF), 2);
            Assert.AreEqual(m_mmu.Read(0xFE00), 3);
            Assert.AreEqual(m_mmu.Read(0xFE9F), 4);
        }

        [Test]
        public void RAMMemory_ReadWriteTest()
        {
            // RAM
            m_mmu.Write(0xC000, 1);
            m_mmu.Write(0xDFFF, 2);

            var ram1 = TestHelpers.GetPrivateField<RAM, byte[]>(m_ram, "m_ram");

            Assert.AreEqual(ram1[0], 1);
            Assert.AreEqual(ram1[^1], 2);

            // HRAM
            m_mmu.Write(0xFF80, 32);
            m_mmu.Write(0xFFFE, 64);

            var hram = TestHelpers.GetPrivateField<RAM, byte[]>(m_ram, "m_hram");

            Assert.AreEqual(hram[0], 32);
            Assert.AreEqual(hram[^1], 64);

            // Interrupt
            m_mmu.Write(0xFFFF, 128);

            var interrupt = TestHelpers.GetPrivateField<RAM, byte>(m_ram, "m_interrupt");

            Assert.AreEqual(interrupt, 128);

            // Echo RAM - Copy of RAM max size of echo is is 7700
            m_mmu.Write(0xE000, 3);
            m_mmu.Write(0xFDFF, 4);
            Assert.AreEqual(ram1[0], 3);
            Assert.AreEqual(ram1[0x1DFF], 4);

            // Empty - Not usable
            m_mmu.Write(0xFEA0, 5);
            m_mmu.Write(0xFF4B, 6);
            m_mmu.Write(0xFF4C, 7);
            m_mmu.Write(0xFF7F, 8);

            // MMU Read
            Assert.AreEqual(m_mmu.Read(0xC000), 3);
            Assert.AreEqual(m_mmu.Read(0xDFFF), 2);
            Assert.AreEqual(m_mmu.Read(0xFDFF), 4);
            Assert.AreEqual(m_mmu.Read(0xFF80), 32);
            Assert.AreEqual(m_mmu.Read(0xFFFE), 64);
            Assert.AreEqual(m_mmu.Read(0xFFFF), 128);
        }

        [Test]
        public void IOMemory_ReadWriteTest()
        {
            // IO
            m_mmu.Write(0xFF00, 1);
            m_mmu.Write(0xFF4B, 2);

            var io1 = TestHelpers.GetPrivateField<IO, byte[]>(m_io, "m_io");

            Assert.AreEqual(io1[0], 1);
            Assert.AreEqual(io1[^1], 2);

            // MMU Read
            Assert.AreEqual(m_mmu.Read(0xFF00), 1);
            Assert.AreEqual(m_mmu.Read(0xFF4B), 2);
        }
    }
}
