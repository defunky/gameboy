using NUnit.Framework;
// ReSharper disable InconsistentNaming

namespace Gameboy.Core.Tests
{
    public class RegistersTests
    {
        private Registers m_registers;

        [SetUp]
        public void SetUp()
        {
            m_registers.AF = 0;
            m_registers.BC = 0;
            m_registers.DE = 0;
            m_registers.HL = 0;
        }

        [Test]
        public void RegisterSingleAssignmentTest()
        {
            // AF
            m_registers.A = 128;
            m_registers.F = 50;
            var expectedAF = combine(m_registers.A, m_registers.F);
            Assert.AreEqual(m_registers.A, 128);
            Assert.AreEqual(m_registers.F, 50);
            Assert.AreEqual(m_registers.AF, expectedAF);


            // BC
            Assert.Zero(m_registers.B);
            Assert.Zero(m_registers.C);
            Assert.Zero(m_registers.BC);

            m_registers.B = 16;
            m_registers.C = 32;
            var expectedBC = combine(m_registers.B, m_registers.C);

            Assert.AreEqual(m_registers.B, 16);
            Assert.AreEqual(m_registers.C, 32);
            Assert.AreEqual(m_registers.BC, expectedBC);

            // DE
            Assert.Zero(m_registers.D);
            Assert.Zero(m_registers.E);
            Assert.Zero(m_registers.DE);

            m_registers.D = 64;
            m_registers.E = 2;
            var expectedDE = combine(m_registers.D, m_registers.E);

            Assert.AreEqual(m_registers.D, 64);
            Assert.AreEqual(m_registers.E, 2);
            Assert.AreEqual(m_registers.DE, expectedDE);

            // HL
            Assert.Zero(m_registers.H);
            Assert.Zero(m_registers.L);
            Assert.Zero(m_registers.HL);

            m_registers.H = 100;
            m_registers.L = 43;
            var expectedHL = combine(m_registers.H, m_registers.L);

            Assert.AreEqual(m_registers.H, 100);
            Assert.AreEqual(m_registers.L, 43);
            Assert.AreEqual(m_registers.HL, expectedHL);

            m_registers.SP = 10;
            m_registers.PC = 20;

            Assert.AreEqual(m_registers.SP, 10);
            Assert.AreEqual(m_registers.PC, 20);
        }

        [Test]
        public void RegisterUnionAssignmentTest()
        {
            // AF
            m_registers.AF = 32818;

            Assert.AreEqual(m_registers.A, 128);
            Assert.AreEqual(m_registers.F, 50);

            // BC
            Assert.Zero(m_registers.B);
            Assert.Zero(m_registers.C);
            Assert.Zero(m_registers.BC);

            m_registers.BC = 4128;

            Assert.AreEqual(m_registers.B, 16);
            Assert.AreEqual(m_registers.C, 32);

            // DE
            Assert.Zero(m_registers.D);
            Assert.Zero(m_registers.E);
            Assert.Zero(m_registers.DE);

            m_registers.DE = 16386;

            Assert.AreEqual(m_registers.D, 64);
            Assert.AreEqual(m_registers.E, 2);

            // HL
            Assert.Zero(m_registers.H);
            Assert.Zero(m_registers.L);
            Assert.Zero(m_registers.HL);

            m_registers.HL = 25643;
            m_registers.L = 43;

            Assert.AreEqual(m_registers.H, 100);
            Assert.AreEqual(m_registers.L, 43);
        }

        private ushort combine(byte x, byte y)
        {
            return (ushort) ((x << 8) | y);
        }

        private (byte x, byte y) split(ushort xy)
        {
            var y = (byte) (xy & 0xFF);
            var x = (byte) ((xy >> 8) & 0xFF);

            return (x, y);
        }
    }
}
