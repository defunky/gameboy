﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Gameboy.Core.Tests
{
    public class TestHelpers
    {
        public static T2 GetPrivateField<T1,T2>(object obj, string fieldName)
        {
            return (T2) typeof(T1).GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Instance).GetValue(obj);
        }
    }
}
