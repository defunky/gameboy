﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gameboy.Core
{
    public class BitHelpers
    {
        /// <summary>
        /// Take two bytes (8 bit) and combine them into one 16 bit address
        /// </summary>
        /// <param name="higher"></param>
        /// <param name="lower"></param>
        /// <returns></returns>
        public static ushort To16Bit(byte higher, byte lower)
        {
            return (ushort) (higher << 8 | lower);
        }

        public static byte SetBit(byte register, byte bit)
        {
            return (byte) (register | 0x01 << bit);
        }

        public static byte SetFlag(byte register, Flags flag)
        {
            return (byte)(register | (byte) flag);
        }

        public static byte ClearBit(byte register, byte bit)
        {
            return (byte)(register & ~(0x01 << bit));
        }

        public static byte ClearFlag(byte register, Flags flag)
        {
            return (byte)(register & (byte) ~(flag));
        }

        public static bool GetBit(byte register, byte bit)
        {
            return (byte)(register & (0x01 << bit)) != 0;
        }

        /// <summary>
        /// Check if the 3rd bit has been carried over to the 4th bit.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public static bool IsHalfCarryAdd(byte x, byte y)
        {
            return (((x & 0xf) + (y & 0xf)) & 0x10) == 0x10;
        }

        /// <summary>
        /// Check if the 4th bit has been carried from the 3th bit.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public static bool IsHalfCarrySub(byte x, byte y)
        {
            return (x & 0x0F) - (y & 0x0F) < 0;
        }

        /// <summary>
        /// Grab the top-most 8 bits from 2-byte register
        /// </summary>
        /// <param name="register"></param>
        /// <returns></returns>
        public static byte GetHighBytes(ushort register)
        {
            return (byte) ((register & 0xff00) >> 8);
        }

        /// <summary>
        /// Grab the bottom-most 8 bits from 2-byte register
        /// </summary>
        /// <param name="register"></param>
        /// <returns></returns>
        public static byte GetLowerBytes(ushort register)
        {
            return (byte)(register & 0x00ff);
        }
    }
}
