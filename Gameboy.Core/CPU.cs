﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

// ReSharper disable RedundantAssignment

namespace Gameboy.Core
{
    internal class Opcode
    {

        public Opcode(string name, Func<int> instruction)
        {
            Name = name;
            Instruction = instruction;
        }

        public string Name { get; set; }

        public Func<int> Instruction { get; set; }
    }

    public class CPU
    {
        private Dictionary<byte, Opcode> m_opcodes;
        private Dictionary<byte, Opcode> m_extendedOpcodes;
        private Registers m_registers;
        private MMU m_mmu;
        private Cartridge m_cartridge;
        private int m_cycle;

        public CPU(string filePath)
        {
            m_registers = new Registers();
            m_opcodes = new Dictionary<byte, Opcode>
            {
                // 0x0
                { 0x00, new Opcode("NOP", () => notImplemented()) },
                { 0x01, new Opcode("LD BC, d16", () => notImplemented()) },
                { 0x02, new Opcode("LD (BC), A", () => notImplemented()) },
                { 0x03, new Opcode("INC BC", () => notImplemented()) },
                { 0x04, new Opcode("INC B", () => inc_r(ref m_registers.B)) },
                { 0x05, new Opcode("DEC B", () => dec_r(ref m_registers.B)) },
                { 0x06, new Opcode("LD B, d8", () => load_r_d8(ref m_registers.B)) },
                { 0x07, new Opcode("RLCA", () => notImplemented()) },
                { 0x08, new Opcode("LD (a16), SP", () => notImplemented()) },
                { 0x09, new Opcode("ADD HL, BC", () => notImplemented()) },
                { 0x0A, new Opcode("LD A, (BC)", () => notImplemented()) },
                { 0x0B, new Opcode("DEC BC", () => notImplemented()) },
                { 0x0C, new Opcode("INC C", () => inc_r(ref m_registers.C)) },
                { 0x0D, new Opcode("DEC C", () => dec_r(ref m_registers.C)) },
                { 0x0E, new Opcode("LD C,d8", () => load_r_d8(ref m_registers.C)) },
                { 0x0F, new Opcode("RRCA", () => notImplemented()) },

                // 0x1
                { 0x10, new Opcode("STOP", () => notImplemented()) },
                { 0x11, new Opcode("LD DE, d16", () => load_r_d16(ref m_registers.DE)) },
                { 0x12, new Opcode("LD (DE), A", () => notImplemented()) },
                { 0x13, new Opcode("INC DE", () => inc_rr(ref m_registers.DE)) },
                { 0x14, new Opcode("INC D", () => notImplemented()) },
                { 0x15, new Opcode("DEC D", () => dec_r(ref m_registers.D)) },
                { 0x16, new Opcode("LD D, D8", () => load_r_d8(ref m_registers.D)) },
                { 0x17, new Opcode("RLA", () => rla()) },
                { 0x18, new Opcode("JR s8", () => jump_s8()) },
                { 0x19, new Opcode("ADD HL, DE", () => notImplemented()) },
                { 0x1A, new Opcode("LD A, (DE)", () => load_r_mem_r(ref m_registers.A, ref m_registers.DE)) },
                { 0x1B, new Opcode("DEC DE", () => notImplemented()) },
                { 0x1C, new Opcode("INC E", () => notImplemented()) },
                { 0x1D, new Opcode("DEC E", () => dec_r(ref m_registers.E)) },
                { 0x1E, new Opcode("LD E, d8", () => load_r_d8(ref m_registers.E)) },
                { 0x1F, new Opcode("RRA", () => notImplemented()) },

                // 0x2
                { 0x20, new Opcode("JR NZ,r8", () => jumpr_r_s8(Flags.N | Flags.Z)) },
                { 0x21, new Opcode("LD HL,d16", () => load_r_d16(ref m_registers.HL)) },
                { 0x22, new Opcode("LD (HL+),A", () => load_hlp_r(ref m_registers.A)) },
                { 0x23, new Opcode("INC HL", () => inc_rr(ref m_registers.HL)) },
                { 0x24, new Opcode("INC H", () => inc_r(ref m_registers.H)) },
                { 0x25, new Opcode("DEC H", () => notImplemented()) },
                { 0x26, new Opcode("LD H,d8", () => notImplemented()) },
                { 0x27, new Opcode("DAA", () => notImplemented()) },
                { 0x28, new Opcode("JR Z,r8", () => jumpr_r_s8(Flags.Z)) },
                { 0x29, new Opcode("ADD HL,HL", () => notImplemented()) },
                { 0x2A, new Opcode("LD A,(HL+)", () => notImplemented()) },
                { 0x2B, new Opcode("DEC HL", () => notImplemented()) },
                { 0x2C, new Opcode("INC L", () => notImplemented()) },
                { 0x2D, new Opcode("DEC L", () => notImplemented()) },
                { 0x2E, new Opcode("LD L,d8", () => load_r_d8(ref m_registers.L)) },
                { 0x2F, new Opcode("CPL", () => notImplemented()) },

                // 0x3
                { 0x30, new Opcode("JR NC,r8", () => notImplemented()) },
                { 0x31, new Opcode("LD SP,d16", () => load_r_d16(ref m_registers.SP)) },
                { 0x32, new Opcode("LD (HL-),A", () => load_hlm_r(ref m_registers.A)) },
                { 0x33, new Opcode("INC SP", () => notImplemented()) },
                { 0x34, new Opcode("INC (HL)", () => notImplemented()) },
                { 0x35, new Opcode("DEC (HL)", () => notImplemented()) },
                { 0x36, new Opcode("LD (HL),d8", () => notImplemented()) },
                { 0x37, new Opcode("SCF", () => notImplemented()) },
                { 0x38, new Opcode("JR C,r8", () => notImplemented()) },
                { 0x39, new Opcode("ADD HL,SP", () => notImplemented()) },
                { 0x3A, new Opcode("LD A,(HL-)", () => notImplemented()) },
                { 0x3B, new Opcode("DEC SP", () => notImplemented()) },
                { 0x3C, new Opcode("INC A", () => notImplemented()) },
                { 0x3D, new Opcode("DEC A", () => dec_r(ref m_registers.A)) },
                { 0x3E, new Opcode("LD A,d8", () => load_r_d8(ref m_registers.A)) },
                { 0x3F, new Opcode("CCF", () => notImplemented()) },

                // 0x4
                { 0x40, new Opcode("LD B,B", () => notImplemented()) },
                { 0x41, new Opcode("LD B,C", () => notImplemented()) },
                { 0x42, new Opcode("LD B,D", () => notImplemented()) },
                { 0x43, new Opcode("LD B,E", () => notImplemented()) },
                { 0x44, new Opcode("LD B,H", () => notImplemented()) },
                { 0x45, new Opcode("LD B,L", () => notImplemented()) },
                { 0x46, new Opcode("LD B,(HL)", () => notImplemented()) },
                { 0x47, new Opcode("LD B,A", () => notImplemented()) },
                { 0x48, new Opcode("LD C,B", () => notImplemented()) },
                { 0x49, new Opcode("LD C,C", () => notImplemented()) },
                { 0x4A, new Opcode("LD C,D", () => notImplemented()) },
                { 0x4B, new Opcode("LD C,E", () => notImplemented()) },
                { 0x4C, new Opcode("LD C,H", () => notImplemented()) },
                { 0x4D, new Opcode("LD C,L", () => notImplemented()) },
                { 0x4E, new Opcode("LD C,(HL)", () => notImplemented()) },
                { 0x4F, new Opcode("LD C,A", () => load_r_r(ref m_registers.C, ref m_registers.A)) },

                // 0x5
                { 0x50, new Opcode("LD D,B", () => notImplemented()) },
                { 0x51, new Opcode("LD D,C", () => notImplemented()) },
                { 0x52, new Opcode("LD D,D", () => notImplemented()) },
                { 0x53, new Opcode("LD D,E", () => notImplemented()) },
                { 0x54, new Opcode("LD D,H", () => notImplemented()) },
                { 0x55, new Opcode("LD D,L", () => notImplemented()) },
                { 0x56, new Opcode("LD D,(HL)", () => notImplemented()) },
                { 0x57, new Opcode("LD D,A", () => load_r_r(ref m_registers.D, ref m_registers.A)) },
                { 0x58, new Opcode("LD E,B", () => notImplemented()) },
                { 0x59, new Opcode("LD E,C", () => notImplemented()) },
                { 0x5A, new Opcode("LD E,D", () => notImplemented()) },
                { 0x5B, new Opcode("LD E,E", () => notImplemented()) },
                { 0x5C, new Opcode("LD E,H", () => notImplemented()) },
                { 0x5D, new Opcode("LD E,L", () => notImplemented()) },
                { 0x5E, new Opcode("LD E,(HL)", () => notImplemented()) },
                { 0x5F, new Opcode("LD E,A", () => notImplemented()) },

                // 0x6
                { 0x60, new Opcode("LD H,B", () => notImplemented()) },
                { 0x61, new Opcode("LD H,C", () => notImplemented()) },
                { 0x62, new Opcode("LD H,D", () => notImplemented()) },
                { 0x63, new Opcode("LD H,E", () => notImplemented()) },
                { 0x64, new Opcode("LD H,H", () => notImplemented()) },
                { 0x65, new Opcode("LD H,L", () => notImplemented()) },
                { 0x66, new Opcode("LD H,(HL)", () => notImplemented()) },
                { 0x67, new Opcode("LD H,A", () => load_r_r(ref m_registers.H, ref m_registers.A)) },
                { 0x68, new Opcode("LD L,B", () => notImplemented()) },
                { 0x69, new Opcode("LD L,C", () => notImplemented()) },
                { 0x6A, new Opcode("LD L,D", () => notImplemented()) },
                { 0x6B, new Opcode("LD L,E", () => notImplemented()) },
                { 0x6C, new Opcode("LD L,H", () => notImplemented()) },
                { 0x6D, new Opcode("LD L,L", () => notImplemented()) },
                { 0x6E, new Opcode("LD L,(HL)", () => notImplemented()) },
                { 0x6F, new Opcode("LD L,A", () => notImplemented()) },

                // 0x7
                { 0x70, new Opcode("LD (HL),B", () => notImplemented()) },
                { 0x71, new Opcode("LD (HL),C", () => notImplemented()) },
                { 0x72, new Opcode("LD (HL),D", () => notImplemented()) },
                { 0x73, new Opcode("LD (HL),E", () => notImplemented()) },
                { 0x74, new Opcode("LD (HL),H", () => notImplemented()) },
                { 0x75, new Opcode("LD (HL),L", () => notImplemented()) },
                { 0x76, new Opcode("HALT", () => notImplemented()) },
                { 0x77, new Opcode("LD (HL),A", () => load_hl_r(ref m_registers.A)) },
                { 0x78, new Opcode("LD A,B", () => load_r_r(ref m_registers.A, ref m_registers.B)) },
                { 0x79, new Opcode("LD A,C", () => notImplemented()) },
                { 0x7A, new Opcode("LD A,D", () => notImplemented()) },
                { 0x7B, new Opcode("LD A,E", () => load_r_r(ref m_registers.A, ref m_registers.E)) },
                { 0x7C, new Opcode("LD A,H", () => load_r_r(ref m_registers.A, ref m_registers.H)) },
                { 0x7D, new Opcode("LD A,L", () => load_r_r(ref m_registers.A, ref m_registers.L)) },
                { 0x7E, new Opcode("LD A,(HL)", () => notImplemented()) },
                { 0x7F, new Opcode("LD A,A", () => notImplemented()) },

                // 0x8
                { 0x80, new Opcode("ADD A,B", () => notImplemented()) },
                { 0x81, new Opcode("ADD A,C", () => notImplemented()) },
                { 0x82, new Opcode("ADD A,E", () => notImplemented()) },
                { 0x83, new Opcode("LD (HL),E", () => notImplemented()) },
                { 0x84, new Opcode("ADD A,H", () => notImplemented()) },
                { 0x85, new Opcode("ADD A,L", () => notImplemented()) },
                { 0x86, new Opcode("ADD A,(HL)", () => add_r_mem_r(ref m_registers.A, ref m_registers.HL)) },
                { 0x87, new Opcode("ADD A,A", () => notImplemented()) },
                { 0x88, new Opcode("ADC A,B", () => notImplemented()) },
                { 0x89, new Opcode("ADC A,C", () => notImplemented()) },
                { 0x8A, new Opcode("ADC A,D", () => notImplemented()) },
                { 0x8B, new Opcode("ADC A,E", () => notImplemented()) },
                { 0x8C, new Opcode("ADC A,H", () => notImplemented()) },
                { 0x8D, new Opcode("ADC A,L", () => notImplemented()) },
                { 0x8E, new Opcode("ADC A,(HL)", () => notImplemented()) },
                { 0x8F, new Opcode("ADC A,A", () => notImplemented()) },

                // 0x9
                { 0x90, new Opcode("SUB B", () => sub_r(ref m_registers.B)) },
                { 0x91, new Opcode("SUB C", () => notImplemented()) },
                { 0x92, new Opcode("SUB D", () => notImplemented()) },
                { 0x93, new Opcode("SUB E", () => notImplemented()) },
                { 0x94, new Opcode("SUB H", () => notImplemented()) },
                { 0x95, new Opcode("SUB L", () => notImplemented()) },
                { 0x96, new Opcode("SUB (HL)", () => notImplemented()) },
                { 0x97, new Opcode("SUB A", () => notImplemented()) },
                { 0x98, new Opcode("SBC A,B", () => notImplemented()) },
                { 0x99, new Opcode("SBC A,C", () => notImplemented()) },
                { 0x9A, new Opcode("SBC A,D", () => notImplemented()) },
                { 0x9B, new Opcode("SBC A,E", () => notImplemented()) },
                { 0x9C, new Opcode("SBC A,H", () => notImplemented()) },
                { 0x9D, new Opcode("SBC A,L", () => notImplemented()) },
                { 0x9E, new Opcode("SBC A,(HL)", () => notImplemented()) },
                { 0x9F, new Opcode("SBC A,A", () => notImplemented()) },

                // 0xA
                { 0xA0, new Opcode("AND B", () => notImplemented()) },
                { 0xA1, new Opcode("AND C", () => notImplemented()) },
                { 0xA2, new Opcode("AND D", () => notImplemented()) },
                { 0xA3, new Opcode("AND E", () => notImplemented()) },
                { 0xA4, new Opcode("AND H", () => notImplemented()) },
                { 0xA5, new Opcode("AND L", () => notImplemented()) },
                { 0xA6, new Opcode("AND (HL)", () => notImplemented()) },
                { 0xA7, new Opcode("AND A", () => notImplemented()) },
                { 0xA8, new Opcode("XOR B", () => notImplemented()) },
                { 0xA9, new Opcode("XOR C", () => notImplemented()) },
                { 0xAA, new Opcode("XOR D", () => notImplemented()) },
                { 0xAB, new Opcode("XOR E", () => notImplemented()) },
                { 0xAC, new Opcode("XOR H", () => notImplemented()) },
                { 0xAD, new Opcode("XOR L", () => notImplemented()) },
                { 0xAE, new Opcode("XOR (HL)", () => notImplemented()) },
                { 0xAF, new Opcode("XOR A", () => xor_r(ref m_registers.A)) },

                // 0xB
                { 0xB0, new Opcode("OR B", () => notImplemented()) },
                { 0xB1, new Opcode("OR C", () => notImplemented()) },
                { 0xB2, new Opcode("OR D", () => notImplemented()) },
                { 0xB3, new Opcode("OR E", () => notImplemented()) },
                { 0xB4, new Opcode("OR H", () => notImplemented()) },
                { 0xB5, new Opcode("OR L", () => notImplemented()) },
                { 0xB6, new Opcode("OR (HL)", () => notImplemented()) },
                { 0xB7, new Opcode("OR A", () => notImplemented()) },
                { 0xB8, new Opcode("CP B", () => notImplemented()) },
                { 0xB9, new Opcode("CP C", () => notImplemented()) },
                { 0xBA, new Opcode("CP D", () => notImplemented()) },
                { 0xBB, new Opcode("CP E", () => notImplemented()) },
                { 0xBC, new Opcode("CP H", () => notImplemented()) },
                { 0xBD, new Opcode("CP L", () => notImplemented()) },
                { 0xBE, new Opcode("CP (HL)", () => cp_mem_rr(ref m_registers.HL)) },
                { 0xBF, new Opcode("CP A", () => notImplemented()) },

                // 0xC
                { 0xC0, new Opcode("RET NZ", () => notImplemented()) },
                { 0xC1, new Opcode("POP BC", () => pop_rr(ref m_registers.BC)) },
                { 0xC2, new Opcode("JP NZ, a16", () => notImplemented()) },
                { 0xC3, new Opcode("JP a16", () => notImplemented()) },
                { 0xC4, new Opcode("CALL NZ, a16", () => notImplemented()) },
                { 0xC5, new Opcode("PUSH BC", () => push_rr(ref m_registers.BC)) },
                { 0xC6, new Opcode("ADD A, d8", () => notImplemented()) },
                { 0xC7, new Opcode("RST 00H", () => notImplemented()) },
                { 0xC8, new Opcode("RET Z", () => notImplemented()) },
                { 0xC9, new Opcode("RET", () => ret()) },
                { 0xCA, new Opcode("JP Z,a16", () => notImplemented()) },
                { 0xCB, new Opcode("PREFIX CB", () => notImplemented()) },
                { 0xCC, new Opcode("CALL Z,a16", () => notImplemented()) },
                { 0xCD, new Opcode("CALL a16", () => call_a16()) },
                { 0xCE, new Opcode("ADC A,d8", () => notImplemented()) },
                { 0xCF, new Opcode("RST 08H", () => notImplemented()) },

                // 0xD
                { 0xD0, new Opcode("RET NC", () => notImplemented()) },
                { 0xD1, new Opcode("POP DE", () => notImplemented()) },
                { 0xD2, new Opcode("JP NC,a16", () => notImplemented()) },
                { 0xD4, new Opcode("CALL NC,a16", () => notImplemented()) },
                { 0xD5, new Opcode("PUSH DE", () => notImplemented()) },
                { 0xD6, new Opcode("SUB d8", () => notImplemented()) },
                { 0xD7, new Opcode("RST 10H", () => notImplemented()) },
                { 0xD8, new Opcode("RET C", () => notImplemented()) },
                { 0xD9, new Opcode("RETI", () => notImplemented()) },
                { 0xDA, new Opcode("JP C,a16", () => notImplemented()) },
                { 0xDC, new Opcode("CALL C,a16", () => notImplemented()) },
                { 0xDE, new Opcode("SBC A,d8", () => notImplemented()) },
                { 0xDF, new Opcode("RST 18H", () => notImplemented()) },

                // 0xE
                { 0xE0, new Opcode("LDH (a8),A", () => load_a8_r(ref m_registers.A)) },
                { 0xE1, new Opcode("POP HL", () => notImplemented()) },
                { 0xE2, new Opcode("LD (C),A", () => load_mem_r_r(ref m_registers.C, ref m_registers.A)) },
                { 0xE5, new Opcode("PUSH HL", () => notImplemented()) },
                { 0xE6, new Opcode("AND d8", () => notImplemented()) },
                { 0xE7, new Opcode("RST 20H", () => notImplemented()) },
                { 0xE8, new Opcode("ADD SP,r8", () => notImplemented()) },
                { 0xE9, new Opcode("JP (HL)", () => notImplemented()) },
                { 0xEA, new Opcode("LD (a16),A", () => load_a16_r(ref m_registers.A)) },
                { 0xEE, new Opcode("XOR d8", () => notImplemented()) },
                { 0xEF, new Opcode("RST 08H", () => notImplemented()) },

                // 0xF
                { 0xF0, new Opcode("LDH A,(a8)", () => load_r_a8(ref m_registers.A)) },
                { 0xF1, new Opcode("POP AF", () => notImplemented()) },
                { 0xF2, new Opcode("LD A,(C)", () => notImplemented()) },
                { 0xF3, new Opcode("DI", () => notImplemented()) },
                { 0xF5, new Opcode("PUSH AF", () => notImplemented()) },
                { 0xF6, new Opcode("OR d8", () => notImplemented()) },
                { 0xF7, new Opcode("RST 30H", () => notImplemented()) },
                { 0xF8, new Opcode("LD HL,SP+r8", () => notImplemented()) },
                { 0xF9, new Opcode("LD SP,HL", () => notImplemented()) },
                { 0xFA, new Opcode("LD A,(a16)", () => notImplemented()) },
                { 0xFB, new Opcode("EI", () => notImplemented()) },
                { 0xFE, new Opcode("CP d8", () => cp_d8()) },
                { 0xFF, new Opcode("RST 38H", () => notImplemented()) },
            };
            m_extendedOpcodes = new Dictionary<byte, Opcode>
            {
                // 0x1
                { 0x11, new Opcode("RL C", () => rotl_r(ref m_registers.C)) },

                // 0x7
                { 0x7C, new Opcode("BIT 7, H", () => bit_b_r(7, ref m_registers.H)) },
            };
            m_cartridge = new Cartridge(filePath);
            m_interrupt = new Interrupt(this);
            gpu = new GPU(m_interrupt);
            var mem = new RAM();
            timer = new Timer(m_interrupt);
            var io = new IO();
            m_mmu = new MMU(m_cartridge, gpu, mem, timer, io, m_interrupt);

            Tick();

        }

        private GPU gpu;
        private Timer timer;
        private Interrupt m_interrupt;
        private StringBuilder b = new StringBuilder();

        public void Tick()
        {
            //m_registers.PC = 0x100;
            while (true)
            {
                var opcode = m_cartridge.Rom[m_registers.PC++];
                Opcode instr;
                if (opcode == 0xCB)
                {
                    // 16bit 0xCB extended instruction
                    var ext = m_cartridge.Rom[m_registers.PC++];
                    instr = m_extendedOpcodes[ext];
                }
                else
                {
                    instr = m_opcodes[opcode];
                }

                //if(b.Length > 0)
                b.AppendLine($"OPCODE: ${instr.Name} - 0x{opcode.ToString("X2")}");
                if (b.Length > int.MaxValue /5)
                {
                    Trace.Write(b.ToString());
                    b.Clear();
                }

                //Trace.WriteLine($"OPCODE: ${instr.Name} - 0x{opcode.ToString("X2")}");
                m_cycle += instr.Instruction();
                timer.Update(m_cycle);
                gpu.Update(m_cycle);
                m_interrupt.Handle();
            }
        }

        public void Execute(byte b)
        {
            var instr = m_opcodes[b];
            Trace.WriteLine($"OPCODE: ${instr.Name} - {b.ToString("X2")}");
            instr.Instruction();
        }

        public void ExecuteInterrupt(Enums.Interrupt interrupt)
        {
            m_interrupt.MasterInterrupt = false;
            var value = BitHelpers.ClearBit(m_interrupt.Read(0xFF0F), (byte)interrupt);
            m_interrupt.Write(0xFF0F, value);

            pushToStack(m_registers.PC);
            m_registers.PC = interrupt switch
            {
                Enums.Interrupt.VBLANK => 0x40,
                Enums.Interrupt.LCD => 0x48,
                Enums.Interrupt.Timer => 0x50,
                Enums.Interrupt.Serial => 0x58,
                Enums.Interrupt.Joypad => 0x60,
                _ => throw new ArgumentOutOfRangeException(nameof(interrupt), interrupt, null)
            };
        }

        private void pushToStack(ushort value)
        {
            m_mmu.Write(--m_registers.SP, BitHelpers.GetHighBytes(value));
            m_mmu.Write(--m_registers.SP, BitHelpers.GetLowerBytes(value));
        }

        private ushort popStack()
        {
            var lower = m_mmu.Read(m_registers.SP++);
            var higher = m_mmu.Read(m_registers.SP++);

            return BitHelpers.To16Bit(higher, lower);
        }

        private int inc_r(ref byte register)
        {
            var current = register;
            register++;

            m_registers.F = register == 0x00 ? BitHelpers.SetFlag(m_registers.F, Flags.Z) : BitHelpers.ClearFlag(m_registers.F, Flags.Z);
            m_registers.F = BitHelpers.ClearFlag(m_registers.F, Flags.N);
            m_registers.F = (register & 0x0F) == 0x0F ? BitHelpers.SetFlag(m_registers.F, Flags.H) : BitHelpers.ClearFlag(m_registers.F, Flags.H);

            return 4;
        }

        private int inc_rr(ref ushort register)
        {
            register++;

            return 8;
        }

        private int dec_r(ref byte register)
        {
            var current = register;
            register--;

            m_registers.F = register == 0x00 ? BitHelpers.SetFlag(m_registers.F, Flags.Z) : BitHelpers.ClearFlag(m_registers.F, Flags.Z);
            m_registers.F = BitHelpers.SetFlag(m_registers.F, Flags.N);
            m_registers.F = BitHelpers.IsHalfCarrySub(current, register) ? BitHelpers.SetFlag(m_registers.F, Flags.H) : BitHelpers.ClearFlag(m_registers.F, Flags.H);

            return 4;
        }

        private int load_r_d8(ref byte register)
        {
            var value = m_cartridge.Rom[m_registers.PC++];
            register = value;
            return 8;
        }

        private int jumpr_r_s8(Flags flag)
        {
            var addr = (sbyte)m_cartridge.Rom[m_registers.PC++];
            switch (flag)
            {
                case Flags.N | Flags.Z:
                    if (!BitHelpers.GetBit(m_registers.F, 7))
                    {
                        m_registers.PC += (ushort)addr;
                        return 12;
                    }
                    break;
                case Flags.Z:
                    if (BitHelpers.GetBit(m_registers.F, 7))
                    {
                        m_registers.PC += (ushort)addr;
                        return 12;
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(flag), flag, null);
            }

            return 8;
        }

        private int jump_s8()
        {
            var addr = (sbyte)m_cartridge.Rom[m_registers.PC++];
            m_registers.PC += (ushort)addr;

            return 12;
        }

        private int load_r_d16(ref ushort register)
        {
            var value = BitHelpers.To16Bit(m_cartridge.Rom[m_registers.PC + 1], m_cartridge.Rom[m_registers.PC]);
            register = value;
            m_registers.PC += 2;

            return 12;
        }

        private int load_hlp_r(ref byte register)
        {
            m_mmu.Write(m_registers.HL, register);
            m_registers.HL++;

            return 8;
        }

        private int load_hlm_r(ref byte register)
        {
            m_mmu.Write(m_registers.HL, register);
            m_registers.HL--;

            return 8;
        }

        private int load_hl_r(ref byte register)
        {
            m_mmu.Write(m_registers.HL, register);

            return 8;
        }

        private int xor_r(ref byte register)
        {
            m_registers.A ^= register;
            m_registers.F = m_registers.A == 0x00 ? BitHelpers.SetFlag(m_registers.F, Flags.Z) : BitHelpers.ClearFlag(m_registers.F, Flags.Z);
            m_registers.F = BitHelpers.ClearFlag(m_registers.F, Flags.N);
            m_registers.F = BitHelpers.ClearFlag(m_registers.F, Flags.H);
            m_registers.F = BitHelpers.ClearFlag(m_registers.F, Flags.C);

            return 4;
        }


        private int push_rr(ref ushort register)
        {
            pushToStack(register);

            return 16;
        }

        private int pop_rr(ref ushort register)
        {
            register = popStack();

            return 12;
        }

        private int ret()
        {
            m_registers.PC = popStack();
            return 16;
        }

        private int call_a16()
        {
            var value = BitHelpers.To16Bit(m_cartridge.Rom[m_registers.PC + 1], m_cartridge.Rom[m_registers.PC]);
            m_registers.PC += 2;
            pushToStack(m_registers.PC);
            m_registers.PC = value;

            return 24;
        }


        private int load_a8_r(ref byte register)
        {
            var value = m_cartridge.Rom[m_registers.PC++];
            m_mmu.Write((ushort)(0xFF00 | value), register);

            return 12;
        }

        private int load_r_a8(ref byte register)
        {
            //TODO: SP Broke (off by +2), AF broke (-16). PC off by +2 the value here is broke too
            var value = m_cartridge.Rom[m_registers.PC++];
            register = m_mmu.Read((ushort)(0xFF00 | value));

            return 12;
        }

        private int load_a16_r(ref byte register)
        {
            var value = BitHelpers.To16Bit(m_cartridge.Rom[m_registers.PC + 1], m_cartridge.Rom[m_registers.PC]);
            m_registers.PC += 2;
            m_mmu.Write(value, register);

            return 12;
        }

        private int load_r_r(ref byte to, ref byte from)
        {
            to = from;
            return 4;
        }

        private int load_mem_r_r(ref byte to, ref byte from)
        {
            m_mmu.Write((ushort)(0xFF00 | to), from);
            return 8;
        }

        private int load_r_mem_r(ref byte to, ref ushort from)
        {
            to = m_mmu.Read(from);
            return 8;
        }

        private int add_r_mem_r(ref byte register, ref ushort from)
        {
            var value = m_mmu.Read(from);
            var newValue = (byte)(register + value);

            m_registers.F = register == 0 ? BitHelpers.SetFlag(m_registers.F, Flags.Z) : BitHelpers.ClearFlag(m_registers.F, Flags.Z);
            m_registers.F = BitHelpers.ClearFlag(m_registers.F, Flags.N);
            m_registers.F = BitHelpers.IsHalfCarryAdd(newValue, m_registers.A) ? BitHelpers.SetFlag(m_registers.F, Flags.H) : BitHelpers.ClearFlag(m_registers.F, Flags.H);
            m_registers.F = newValue < m_registers.A ? BitHelpers.SetFlag(m_registers.F, Flags.C) : BitHelpers.ClearFlag(m_registers.F, Flags.C);

            return 8;
        }

        private int sub_r(ref byte register)
        {
            var value = (byte)(m_registers.A - register);

            m_registers.F = value == 0 ? BitHelpers.SetFlag(m_registers.F, Flags.Z) : BitHelpers.ClearFlag(m_registers.F, Flags.Z);
            m_registers.F = BitHelpers.SetFlag(m_registers.F, Flags.N);
            // Half Carry flag set only if the value to subtract is higher than the source. e.g. 4 - 5
            // TODO: Check if this works
            m_registers.F = BitHelpers.IsHalfCarrySub(value, m_registers.A) ? BitHelpers.SetFlag(m_registers.F, Flags.H) : BitHelpers.ClearFlag(m_registers.F, Flags.H);
            m_registers.F = m_registers.A < value ? BitHelpers.SetFlag(m_registers.F, Flags.C) : BitHelpers.ClearFlag(m_registers.F, Flags.C);

            m_registers.A = value;
            return 4;
        }

        private int rla()
        {
            // Grab current Carry flag
            var prevCarry = BitHelpers.GetBit(m_registers.F, 7);
            // Is the msb set, if so we need to carry it
            var isCarry = BitHelpers.GetBit(m_registers.A, 7);

            // Bitshift left
            m_registers.A = (byte)(m_registers.A << 1);

            // If there was carry previously, set it.
            if (prevCarry) m_registers.A = BitHelpers.SetBit(m_registers.A, 0);

            m_registers.F = BitHelpers.ClearFlag(m_registers.F, Flags.Z);
            m_registers.F = BitHelpers.ClearFlag(m_registers.F, Flags.N);
            m_registers.F = BitHelpers.ClearFlag(m_registers.F, Flags.H);

            // Update Carry flag if there is a carry
            m_registers.F = isCarry ? BitHelpers.SetFlag(m_registers.F, Flags.C) : BitHelpers.ClearFlag(m_registers.F, Flags.C);

            return 4;
        }

        // 0xCB
        private int rotl_r(ref byte register)
        {
            // Grab current Carry flag
            var prevCarry = BitHelpers.GetBit(m_registers.F, 7);
            // Is the msb set, if so we need to carry it
            var isCarry = BitHelpers.GetBit(register, 7);

            // Bitshift left
            register = (byte)(register << 1);

            // If there was carry previously, set it.
            if (prevCarry) register = BitHelpers.SetBit(register, 0);

            m_registers.F = register == 0x00 ? BitHelpers.SetFlag(m_registers.F, Flags.Z) : BitHelpers.ClearFlag(m_registers.F, Flags.Z);
            m_registers.F = BitHelpers.ClearFlag(m_registers.F, Flags.N);
            m_registers.F = BitHelpers.ClearFlag(m_registers.F, Flags.H);

            // Update Carry flag if there is a carry
            m_registers.F = isCarry ? BitHelpers.SetFlag(m_registers.F, Flags.C) : BitHelpers.ClearFlag(m_registers.F, Flags.C);

            return 8;
        }

        private int bit_b_r(byte bit, ref byte register)
        {
            var isSet = BitHelpers.GetBit(register, bit);
            m_registers.F = !isSet ? BitHelpers.SetFlag(m_registers.F, Flags.Z) : BitHelpers.ClearFlag(m_registers.F, Flags.Z);
            m_registers.F = BitHelpers.SetFlag(m_registers.F, Flags.H);
            m_registers.F = BitHelpers.ClearFlag(m_registers.F, Flags.N);

            return 8;
        }


        private int cp_d8()
        {
            cp(m_cartridge.Rom[m_registers.PC++]);
            return 8;
        }

        private int cp_mem_rr(ref ushort register)
        {
            var val = m_mmu.Read(register);
            var res = (byte)(m_registers.A - val);

            m_registers.F = res == 0x00 | m_registers.A == val ? BitHelpers.SetFlag(m_registers.F, Flags.Z) : BitHelpers.ClearFlag(m_registers.F, Flags.Z);
            m_registers.F = BitHelpers.SetFlag(m_registers.F, Flags.N);
            m_registers.F = BitHelpers.IsHalfCarrySub(m_registers.A, val) ? BitHelpers.SetFlag(m_registers.F, Flags.H) : BitHelpers.ClearFlag(m_registers.F, Flags.H);
            m_registers.F = m_registers.A < val ? BitHelpers.SetFlag(m_registers.F, Flags.C) : BitHelpers.ClearFlag(m_registers.F, Flags.C);

            return 4;
        }

        private int cp_r(ref byte register)
        {
            cp(register);
            return 4;
        }

        private void cp(byte register)
        {
            var res = (byte)(m_registers.A - register);
            m_registers.F = register == 0x00 | m_registers.A == register ? BitHelpers.SetFlag(m_registers.F, Flags.Z) : BitHelpers.ClearFlag(m_registers.F, Flags.Z);
            m_registers.F = BitHelpers.SetFlag(m_registers.F, Flags.N);
            m_registers.F = BitHelpers.IsHalfCarrySub(res, register) ? BitHelpers.SetFlag(m_registers.F, Flags.H) : BitHelpers.ClearFlag(m_registers.F, Flags.H);
            m_registers.F = m_registers.A < register ? BitHelpers.SetFlag(m_registers.F, Flags.C) : BitHelpers.ClearFlag(m_registers.F, Flags.C);
        }

        private int notImplemented(params object[] arguments)
        {
            Trace.Write(b.ToString());
            Trace.WriteLine($"Error: Not Implemented");
            throw new NotImplementedException();
        }
    }
}
