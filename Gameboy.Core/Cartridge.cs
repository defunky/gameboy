﻿using System.IO;

namespace Gameboy.Core
{
    public class Cartridge : IMemory
    {
        public byte[] Rom { get; }

        private const ushort BANK_OFFSET = 0x4000;
        private const ushort EXTERN_OFFSET = 0xA000;
        private byte[] m_romBank;
        private byte[] m_romBank2;
        private byte[] m_externRam;

        public Cartridge(string filePath)
        {
            if(!string.IsNullOrWhiteSpace(filePath))
                Rom = File.ReadAllBytes(filePath);
            m_romBank = new byte[16 * 1024];    // 0000 ROM - bank #0
            m_romBank2 = new byte[16 * 1024];   // 4000 - switchable ROM bank
            m_externRam = new byte[8 * 1024];   // A000 switchable ram bank
        }

        public void Write(ushort address, byte value)
        {
            switch (address)
            {
                case < 0x4000:
                    m_romBank[address] = value;
                    break;
                case >= 0x4000 and < 0xA000:
                    m_romBank2[address - BANK_OFFSET] = value;
                    break;
                default:
                    m_externRam[address - EXTERN_OFFSET] = value;
                    break;
            }
        }

        public byte Read(ushort address)
        {
            return address switch
            {
                < 0x4000 => m_romBank[address],
                >= 0x4000 and < 0xA000 => m_romBank2[address - BANK_OFFSET],
                _ => m_externRam[address - EXTERN_OFFSET]
            };
        }
    }
}
