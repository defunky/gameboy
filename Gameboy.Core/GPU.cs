﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gameboy.Core
{
    enum LCDStatus
    {
        HBLANK = 0x00,
        VBLANK = 0x01,
        SearchingOAM = 0x02,
        Transferring = 0x03,
    }

    public class GPU : IMemory
    {
        private const ushort V_OFFSET = 0x8000;
        private const ushort OAM_OFFSET = 0xFE00;
        private const ushort LCD_OFFSET = 0xFF40;
        private const ushort LCD_CONTROL_REGISTER = 0xFF40;
        private const ushort LCD_STATUS_REGISTER = 0xFF41;


        private const ushort CYCLE_SEARCH_OAM = 80;
        private const ushort CYCLE_TRANSFER = 172;
        private const ushort CYCLE_HBLANK = 204;
        private const ushort CYCLE_VBLANK = 465;


        private byte[] m_vram;
        private byte[] m_oam;
        private byte[] m_lcd;

        private int m_scanlinesCycle;
        private readonly Interrupt m_interrupt;

        public GPU(Interrupt interrupt)
        {
            m_interrupt = interrupt;
            m_vram = new byte[8 * 1024]; // 8000 - video ram
            m_oam = new byte[0xA0];      // FE00 - Sprite Attribute memory
            m_lcd = new byte[12]; // Special LCD registers
        }

        public void Update(int cycle)
        {
            if (!isLCDEnabled()) return;

            m_scanlinesCycle += cycle;

            var status = (LCDStatus)(Read(LCD_STATUS_REGISTER) & 0x03);

            switch (status)
            {
                case LCDStatus.HBLANK:
                    if (m_scanlinesCycle >= CYCLE_HBLANK)
                    {
                        m_scanlinesCycle -= CYCLE_HBLANK;
                        m_lcd[0xFF44 - LCD_OFFSET] += 1;
                        if (Read(0xFF44) == 144)
                        {
                            SetLCDStatus(LCDStatus.VBLANK);
                            // TODO: Implement Render function
                            //Render();
                            m_interrupt.Request(Enums.Interrupt.VBLANK);
                            if (BitHelpers.GetBit(Read(LCD_CONTROL_REGISTER), 4))
                            {
                                m_interrupt.Request(Enums.Interrupt.LCD);
                            }
                        }
                    }
                    else
                    {
                        SetLCDStatus(LCDStatus.SearchingOAM);
                        if (BitHelpers.GetBit(Read(LCD_CONTROL_REGISTER), 5))
                        {
                            m_interrupt.Request(Enums.Interrupt.LCD);
                        }
                    }

                    break;
                case LCDStatus.VBLANK:
                    if (m_scanlinesCycle >= CYCLE_VBLANK)
                    {
                        m_scanlinesCycle -= CYCLE_VBLANK;

                        m_lcd[0xFF44 - LCD_OFFSET] += 1;
                        if (Read(0xFF44) == 154)
                        {
                            SetLCDStatus(LCDStatus.SearchingOAM);
                            m_lcd[0xFF44 - LCD_OFFSET] = 0x00;
                            if (BitHelpers.GetBit(Read(LCD_CONTROL_REGISTER), 5))
                            {
                                m_interrupt.Request(Enums.Interrupt.LCD);
                            }
                        }
                    }
                    break;
                case LCDStatus.SearchingOAM:
                    if (m_scanlinesCycle >= CYCLE_SEARCH_OAM)
                    {

                        m_scanlinesCycle -= CYCLE_SEARCH_OAM;
                        SetLCDStatus(LCDStatus.Transferring);
                    }
                    break;
                case LCDStatus.Transferring:
                    if (m_scanlinesCycle >= CYCLE_TRANSFER)
                    {
                        m_scanlinesCycle -= CYCLE_TRANSFER;
                        // TODO: Implement Render function
                        //RenderScanline();
                        SetLCDStatus(LCDStatus.HBLANK);
                        if (BitHelpers.GetBit(Read(LCD_CONTROL_REGISTER), 3))
                        {
                            m_interrupt.Request(Enums.Interrupt.LCD);
                        }
                    }

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            // Check Conicidence flag and trigger interrupt if needed
            if (Read(0xFF45) == Read(0xFF44))
            {
                var statusRegister = Read(LCD_STATUS_REGISTER);
                Write(LCD_STATUS_REGISTER, BitHelpers.SetBit(statusRegister, 2));
                if (BitHelpers.GetBit(Read(LCD_STATUS_REGISTER), 2))
                {
                    m_interrupt.Request(Enums.Interrupt.LCD);
                }
                else
                {
                    Write(LCD_STATUS_REGISTER, BitHelpers.ClearBit(statusRegister, 2));
                }
            }

            //if (m_scanlinesCycle <= 0)
            //{
            //    var currentScanline = (byte)(Read(0xFF44) + 1);
            //    m_lcd[0xFF40 - LCD_OFFSET] = currentScanline;

            //    // it takes 456 cock cyles to draw 1 scanline
            //    m_scanlinesCycle = 456;

            //    if (currentScanline == 144)
            //    {
            //        m_interrupt.Request(Enums.Interrupt.VBLANK);
            //    }
            //    else if (m_scanlinesCycle > 153)
            //    {
            //        m_lcd[0xFF44 - LCD_OFFSET] = 0;
            //    }
            //    else if (currentScanline < 144)
            //    {
            //        // drawScanline();
            //    }
            //}
        }


        public void Write(ushort address, byte value)
        {
            switch (address)
            {
                case >= 0xFE00 and <= 0xFE9F:
                    m_oam[address - OAM_OFFSET] = value;
                    break;
                case 0xFF44:
                    m_lcd[0xFF44 - LCD_OFFSET] = 0x00;
                    break;
                case >= 0xFF40 and <= 0xFF4B:
                    m_lcd[address - LCD_OFFSET] = value;
                    break;
                default:
                    m_vram[address - V_OFFSET] = value;
                    break;
            }
        }

        public byte Read(ushort address)
        {
            return address switch
            {
                >= 0xFE00 and <= 0xFE9F => m_oam[address - OAM_OFFSET],
                >= 0xFF40 and <= 0xFF4B => m_lcd[address - LCD_OFFSET],
                _ => m_vram[address - V_OFFSET]
            };
        }

        private bool isLCDEnabled()
        {
            return BitHelpers.GetBit(Read(0xFF40), 7);
        }

        private void SetLCDStatus(LCDStatus status)
        {
            m_lcd[LCD_CONTROL_REGISTER - LCD_OFFSET] = (byte)((m_lcd[LCD_CONTROL_REGISTER - LCD_OFFSET] & ~0x03) | (byte)status);
        }
    }
}
