﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gameboy.Core
{
    public interface IMemory
    {
        void Write(ushort address, byte value);
        byte Read(ushort address);
    }
}
