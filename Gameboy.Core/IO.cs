﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gameboy.Core
{
    public class IO : IMemory
    {
        private const ushort OFFSET = 0xFF00;
        private readonly byte[] m_io;

        public IO()
        {
            m_io = new byte[0x4c]; // FF00 - IO ports
        }

        public void Write(ushort address, byte value)
        {
            m_io[address - OFFSET] = value;
        }

        public byte Read(ushort address)
        {
            return m_io[address - OFFSET];
        }
    }
}
