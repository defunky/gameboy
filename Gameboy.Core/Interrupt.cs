﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gameboy.Core
{
    namespace Enums
    {
        [Flags]
        public enum Interrupt : byte
        {
            VBLANK = 0,
            LCD = 1,
            Timer = 2,
            Serial = 3,
            Joypad = 4,
        }
    }
    
    public class Interrupt : IMemory
    {
        private const ushort INTERRUPT_REQUEST = 0xFF0F;
        private const ushort INTERRUPT_ENABLED = 0xFFFF;

        private readonly CPU m_cpu;
        private byte m_interruptRequest;
        private byte m_interruptEnabled;

        public bool MasterInterrupt { get; set; }

        public Interrupt(CPU cpu)
        {
            m_cpu = cpu;
            MasterInterrupt = true;
        }

        public void Request(Enums.Interrupt interrupt)
        {
            var value = BitHelpers.SetBit(Read(INTERRUPT_REQUEST), (byte)interrupt);
            Write(INTERRUPT_REQUEST, value); 
        }

        public void Handle()
        {
            if (!MasterInterrupt)
                return;

            if (m_interruptRequest > 0x00F)
            {
                foreach (Enums.Interrupt interrupt in Enum.GetValues(typeof(Enums.Interrupt)))
                {
                    if (BitHelpers.GetBit(m_interruptEnabled, (byte)interrupt) &&
                        BitHelpers.GetBit(m_interruptRequest, (byte)interrupt))
                    {
                        m_cpu.ExecuteInterrupt(interrupt);
                    }
                }
            }
        }

        public void Write(ushort address, byte value)
        {
            switch (address)
            {
                case INTERRUPT_REQUEST:
                    m_interruptRequest = value;
                    break;
                case INTERRUPT_ENABLED:
                    m_interruptEnabled = value;
                    break;
            }
        }

        public byte Read(ushort address)
        {
            return address switch
            {
                INTERRUPT_REQUEST => m_interruptRequest,
                INTERRUPT_ENABLED => m_interruptEnabled,
                _ => throw new ArgumentOutOfRangeException()
            };
        }
    }
}
