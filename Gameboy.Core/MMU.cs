﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gameboy.Core
{
    /**
     * Memory Management Unit - Memory Map
     * Cartridge
     * --------
     * 0x0000 - 0x3FFF- 16kb ROM Bank
     * 0x4000 - 0x7FFF - 16kB switchable ROM Bank
     *
     * GPU
     * --------
     * 0x8000 - 0x9FFF - 8kb Video RAM - 
     *
     * Cartridge
     * ---------
     * 0xA000 - 0xBFFF - 8kb switchable RAM bank (Cartridge RAM for saves)
     *
     * RAM
     * ------
     * 0xC000 - 0xDFFF - 8kb internal RAM
     * 0xE000 - 0xFDFF - Echo of internal RAM (not usable)
     *
     * GPU
     * ----
     * 0xFE00 - 0xFE9F - Sprite Attribute Memory (OAM)
     *
     * RAM
     * ------
     * 0xFEA0 - 0xFF4B - Empty but unusable for I/O
     *
     * IO
     * ------
     * 0xFF00 - 0xFF4B - I/O ports
     * 0xFF40 - 0xFF4B - LCD Registers
     *
     * RAM
     * ------
     * 0xFF4C - 0xFF7F - Empty but unusable for I/O 
     * 0xFF80 - 0xFFFE Internal (High) RAM
     * 0xFFFF - Interrupt Enable Register 
     *
     */
    public class MMU : IMemory
    {
        private IMemory[] m_memory;

        public MMU(Cartridge cartridge, GPU gpu, RAM ram, Timer timer, IO io, Interrupt interrupt)
        {
            m_memory = new IMemory[0xFFFF + 1];
            // Rom Banks
            initialize(0x0000, 0x8000, cartridge);
            // VRAM
            initialize(0x8000, 0xA000, gpu);
            // RAM Bank
            initialize(0xA000, 0xC000, cartridge);
            // RAM
            initialize(0xC000, 0xFE00, ram);
            // OAM
            initialize(0xFE00, 0xFEA0, gpu);
            // Unused
            initialize(0xFEA0, 0xFF00, ram);
            // I/O Ports
            initialize(0xFF04, 0xFF08, timer);
            initialize(0xFF08, 0xFF4C, io); // TODO: Replace this with other IO classes
            initialize(0xFF40, 0xFF4C, gpu);
            // RAM
            initialize(0xFF4C, 0xFFFF, ram);
            // Interrupts
            initialize(0xFFFF, 0xFFFF+1, interrupt);
            initialize(0xFF0F, 0xFF10, interrupt);
        }

        private void initialize(int start, int end, IMemory type)
        {
            // TODO: Replace this shit something more memory efficient
            for (int i = start; i < end; i++)
            {
                m_memory[i] = type;
            }
        }

        public void Write(ushort address, byte value)
        {
            m_memory[address].Write(address, value);
        }

        public byte Read(ushort address)
        {
            return m_memory[address].Read(address);
        }
    }
}
