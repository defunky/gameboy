﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gameboy.Core
{
    public class RAM : IMemory
    {
        private ushort RAM_OFFSET = 0xC000;
        private ushort RAM_ECHO_OFFSET = 0xE000;
        private ushort HRAM_OFFSET = 0xFF80;

        private byte[] m_ram;
        private byte[] m_hram;

        // 0xC000 - 0xDFFF - 8kb internal RAM
        // 0xE000 - 0xFDFF - Echo of internal RAM(not usable)  max size 7700
        // 0xFEA0 - 0xFF4B - Empty but unusable for I/O
        // 0xFF4C - 0xFF7F - Empty but unusable for I/O
        // 0xFF80 - 0xFFFE Internal(High) RAM
        // 0xFFFF - Interrupt Enable Register
        public RAM()
        {
            m_ram = new byte[8 * 1024];         // C000 - work ram
                                                // E000 - Copy of work ram shouldn't  be used.
            m_hram = new byte[0x7F];            // FF80 - High ram (acts like cache)
        }

        public void Write(ushort address, byte value)
        {
            switch (address)
            {
                case < 0xE000:
                    m_ram[address - RAM_OFFSET] = value;
                    break;
                case >= 0xE000 and < 0xFE00:
                    m_ram[address - RAM_ECHO_OFFSET] = value;
                    break;
                case >= 0xFE00 and < 0xFF80:
                    //Trace.WriteLine("Writing to Empty Memory");
                    break;
                case >= 0xFF80 and < 0xFFFF:
                    m_hram[address - HRAM_OFFSET] = value;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public byte Read(ushort address)
        {
            switch (address)
            {
                case < 0xE000:
                    return m_ram[address - RAM_OFFSET];
                case >= 0xE000 and < 0xFE00:
                    return m_ram[address - RAM_ECHO_OFFSET];
                case >= 0xFF80 and < 0xFFFF:
                    return m_hram[address - HRAM_OFFSET];
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
