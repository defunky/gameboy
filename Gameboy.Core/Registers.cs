﻿using System;
using System.Runtime.InteropServices;

namespace Gameboy.Core
{
    [Flags]
    public enum Flags : byte
    {
        C = 0x10, // Carry
        H = 0x20, // Half Carry
        N = 0x40, // Subtract
        Z = 0x80, // Zero
    }

    /// <summary>
    /// 8 8-Bit registers combined to create 4 16-bit registers
    /// A (accumulator) + F (flags) = AF
    /// B + C = BC
    /// D + E = DE
    /// H + L = HL
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    public struct Registers
    {
        [FieldOffset(0)] public ushort AF;
        [FieldOffset(0)] public byte F;
        [FieldOffset(1)] public byte A;

        [FieldOffset(2)] public ushort BC;
        [FieldOffset(2)] public byte C;
        [FieldOffset(3)] public byte B;

        [FieldOffset(4)] public ushort DE;
        [FieldOffset(4)] public byte E;
        [FieldOffset(5)] public byte D;

        [FieldOffset(6)] public ushort HL;
        [FieldOffset(6)] public byte L;
        [FieldOffset(7)] public byte H;

        [FieldOffset(8)] public ushort SP; // Stack pointer
        [FieldOffset(10)] public ushort PC; // Program counter
    }
}
