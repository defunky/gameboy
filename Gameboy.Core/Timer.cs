﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gameboy.Core
{
    public class Timer : IMemory
    {
        private const ushort DIVIDER_REGISTER = 0xFF04; // Similar to timer but doesn't interrupt
        private const ushort TIMER = 0xFF05; // Address of the timer that counts up
        private const ushort TIMER_MODULATOR =  0xFF06; // Address of modulator which tells timer what to reset to
        private const ushort TIMER_CONTROL = 0xFF07; // Address of controller which specifies the frequency the timer is set at
        private const int CLOCK_SPEED = 4194304;

        private byte[] m_timingRegisters;
        private readonly Dictionary<byte, int> m_frequencies;
        private int m_timer;
        private int m_divider;
        private readonly Interrupt m_interrupt;

        public Timer(Interrupt interrupt)
        {
            m_interrupt = interrupt;
            m_timingRegisters = new byte[4];
            m_frequencies = new Dictionary<byte, int>()
            {
                // value = clock speed / frequency
                {0b00, CLOCK_SPEED / 4096},   // 1024
                {0b01, CLOCK_SPEED / 262144}, // 16 
                {0b10, CLOCK_SPEED / 65536},  // 64
                {0b11, CLOCK_SPEED / 16382},  // 256
            };
            m_timer = CLOCK_SPEED / 4096;
        }

        public void Update(int cycle)
        {
            updateDivider(cycle);

            m_timer -= cycle;
            // Check 3rd bit if clock is enabled
            if (BitHelpers.GetBit(Read(TIMER_CONTROL), 2))
            {
                if (m_timer <= 0)
                {
                    updateFrequency();

                    var currentTimer = Read(TIMER);
                    if (currentTimer == 0xFF)
                    {
                        // Reset timer to value stored in modulator
                        Write(TIMER, Read(TIMER_MODULATOR));

                        m_interrupt.Request(Enums.Interrupt.Timer);
                    }
                    else
                    {
                        Write(TIMER, (byte)(currentTimer + 1));
                    }
                }
            }
        }

        private void updateDivider(int cycle)
        {
            // TODO: Double check this, might have to directly modify ROM address rather than fake memory.
            // Divider counts up and when it overflows it resets without interrupt unlike timer.
            // Trap the divder in rom too?
            m_divider += cycle;
            if (m_divider >= 0xFF)
            {
                m_divider = 0;
                m_timingRegisters[DIVIDER_REGISTER - DIVIDER_REGISTER]++;
            }
        }

        private void updateFrequency()
        {
            var freq = readFrequency();
            m_timer = m_frequencies[freq];
        }

        private byte readFrequency()
        {
            // 3 bit register, first 2 bits specifies frequency.
           return (byte)(Read(TIMER_CONTROL) & 0x3);
        }

        public void Write(ushort address, byte value)
        {
            switch (address)
            {
                case DIVIDER_REGISTER:
                    m_timingRegisters[address - DIVIDER_REGISTER] = 0x00;
                    break;
                case TIMER_CONTROL:
                    var freq = readFrequency();
                    m_timingRegisters[address - DIVIDER_REGISTER] = 0x00; 
                    if(freq != readFrequency()) updateFrequency();
                    break;
                default:
                    m_timingRegisters[address - DIVIDER_REGISTER] = value;
                    break;
            }
        }

        public byte Read(ushort address)
        {
            return m_timingRegisters[address - DIVIDER_REGISTER];
        }
    }
}
