﻿using System;
using Gameboy.Core;
using OpenTK.Windowing.Desktop;

namespace Gameboy.Display
{
    public class Program
    {
        private const int WIDTH = 800;
        private const int HEIGHT = 600;
        public static void Main(string[] args)
        {
            byte c = 128;
            byte d = 50;
            byte e = (byte)(c + d);
            Registers register;
            register.AF = 32818;
            //register.A = 128;
            //register.F = 50;

            var gameWindowSettings = new GameWindowSettings();
            var nativeSettings = new NativeWindowSettings
            {
                Size = new OpenTK.Mathematics.Vector2i(800, 600),
                Title = "Gameboy 😎"
            };
            using var game = new Game(gameWindowSettings, nativeSettings);
            // Is it needed?
            game.Run();
        }
    }
}
