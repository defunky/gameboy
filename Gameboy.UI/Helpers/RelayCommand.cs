﻿using System;
using System.Windows.Input;

namespace Gameboy.UI.Helpers
{
    public class RelayCommand : ICommand
    {
        private readonly Action m_execute;
        private readonly Predicate<object> m_canExecute;

        public RelayCommand(Action execute, Predicate<object> canExecute = null)
        {
            m_execute = execute;
            m_canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return m_canExecute == null || m_canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            m_execute();
        }

        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }
    }
}
