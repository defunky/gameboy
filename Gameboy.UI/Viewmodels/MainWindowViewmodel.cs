﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gameboy.Core;
using Gameboy.UI.Helpers;
using Microsoft.Win32;

namespace Gameboy.UI.Viewmodels
{
    public class MainWindowViewmodel
    {
        public MainWindowViewmodel()
        {
            OpenRom = new RelayCommand(openRom);
      //      new Cartridge(@"C:\Users\Admin\source\repos\Gameboy\roms\Tetris (W) (V1.0) [!].gb");
         //   new Cartridge(@"C:\Users\Admin\source\repos\Gameboy\roms\cpu_instrs.gb");
        }

        public RelayCommand OpenRom { get; set; }

        private void openRom()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.ShowDialog();
            Task.Run(() => new CPU(openFileDialog.FileName));
            //byte[] fileBytes = File.ReadAllBytes(openFileDialog.FileName);
            // if (openFileDialog.ShowDialog() == true)
            //  txtEditor.Text = File.ReadAllText(openFileDialog.FileName);
        }

        public Test Test { get; set; } = new Test();
    }
}
